/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ntc.swing02;

/**
 *
 * @author L4ZY
 */
public class Triangle extends Shape{
    private double h;
    private double b;
    
    public Triangle(double h,double b) {
        this.h = h;
        this.b = b;

    }

    public double getH() {
        return h;
    }

    public void setH(double h) {
        this.h = h;
    }

    public double getB() {
        return b;
    }

    public void setB(double b) {
        this.b = b;
    }
    
    @Override
    public double calArea() {

        return 0.5 * h * b;

    }

    @Override
    public double calPerimeter() {

        return b * 3;

    }

}
