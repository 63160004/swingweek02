/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ntc.swing02;

/**
 *
 * @author L4ZY
 */
public abstract class Shape {


    public abstract double calArea();
    public abstract double calPerimeter();

}
