/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ntc.swing02;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JOptionPane;

/**
 *
 * @author L4ZY
 */
public class Shapecalculator extends JFrame {

    //circle
    JLabel lbCircle, lbCircleout;
    JTextField txtR;
    //triangle
    JLabel lbTriangle, lbTriangleout;
    JTextField txtH, txtB;
    //rectangle
    JLabel lbRectangle, lbRectangleout;
    JTextField txtW, txtL;
    //square
    JLabel lbSquare, lbSquareout;
    JTextField txtS;

    JButton btnCal, btnClear, btnCal_All, btnExit;

    public Shapecalculator() {

        super("Shape Calculator");

        this.setSize(725, 725);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);
        //circle
        lbCircle = new JLabel("Circle :", JLabel.TRAILING);
        lbCircle.setSize(110, 30);
        lbCircle.setLocation(5, 20);
        lbCircle.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 20));
        this.add(lbCircle);

        txtR = new JTextField("Radius (r)");
        txtR.setForeground(Color.GRAY);
        txtR.setSize(100, 30);
        txtR.setLocation(120, 20);

        txtR.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                if (txtR.getText().equals("Radius (r)")) {
                    txtR.setText("");

                }
            }

            @Override
            public void focusLost(FocusEvent e) {
                if (txtR.getText().isEmpty()) {
                    txtR.setForeground(Color.GRAY);
                    txtR.setText("Radius (r)");
                }
            }
        });
        this.add(txtR);

        btnCal = new JButton("Calculate");
        btnCal.setSize(100, 30);
        btnCal.setLocation(370, 20);
        btnCal.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strRadius = txtR.getText();
                    double r = Double.parseDouble(strRadius);
                    Circle circle = new Circle(r);
                    lbCircleout.setText("Circle: Radius = " + String.format("%.2f", r)
                            + " Area = " + String.format("%.2f", circle.calArea())
                            + " Perimeter = " + String.format("%.2f", circle.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(Shapecalculator.this, "Error: Please input number!",
                             "Error", JOptionPane.ERROR_MESSAGE);
                    txtR.setText("");
                    txtR.requestFocus();
                }
            }
        });

        this.add(btnCal);

        lbCircleout = new JLabel("Circle : Radius = ..........   Area = .......... Perimeter = ..........");
        lbCircleout.setSize(700, 30);
        lbCircleout.setLocation(30, 60);
        lbCircleout.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 15));
        this.add(lbCircleout);

        //triangle
        lbTriangle = new JLabel("Triangle :", JLabel.TRAILING);
        lbTriangle.setSize(110, 30);
        lbTriangle.setLocation(5, 100);
        lbTriangle.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 20));
        this.add(lbTriangle);

        txtH = new JTextField("Height (h)");
        txtH.setForeground(Color.GRAY);
        txtH.setSize(100, 30);
        txtH.setLocation(120, 100);
        txtH.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                if (txtH.getText().equals("Height (h)")) {
                    txtH.setText("");

                }
            }

            @Override
            public void focusLost(FocusEvent e) {
                if (txtH.getText().isEmpty()) {
                    txtH.setForeground(Color.GRAY);
                    txtH.setText("Height (h)");
                }
            }
        });
        this.add(txtH);

        txtB = new JTextField("Base (b)");
        txtB.setForeground(Color.GRAY);
        txtB.setSize(100, 30);
        txtB.setLocation(230, 100);
        txtB.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                if (txtB.getText().equals("Base (b)")) {
                    txtB.setText("");

                }
            }

            @Override
            public void focusLost(FocusEvent e) {
                if (txtB.getText().isEmpty()) {
                    txtB.setForeground(Color.GRAY);
                    txtB.setText("Base (b)");
                }
            }
        });
        this.add(txtB);

        btnCal = new JButton("Calculate");
        btnCal.setSize(100, 30);
        btnCal.setLocation(370, 100);
        btnCal.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strBase = txtH.getText();
                    String strHeight = txtB.getText();
                    double h = Double.parseDouble(strBase);
                    double b = Double.parseDouble(strHeight);
                    Triangle triangle = new Triangle(h, b);
                    lbTriangleout.setText("Triangle: Base = " + String.format("%.2f", b)
                            + " Height = " + String.format("%.2f", h)
                            + " Area = " + String.format("%.2f", triangle.calArea())
                            + " Perimeter = " + String.format("%.2f", triangle.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(Shapecalculator.this, "Error: Please input number!",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    txtB.setText("");
                    txtH.setText("");
                    txtB.requestFocus();
                }
            }
        });
        this.add(btnCal);

        lbTriangleout = new JLabel("Triangle : Height = ..........   Base = .......... Area = .......... Perimeter = ..........");
        lbTriangleout.setSize(700, 30);
        lbTriangleout.setLocation(30, 140);
        lbTriangleout.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 15));
        this.add(lbTriangleout);

        //rectangle
        lbRectangle = new JLabel("Rectangle :", JLabel.TRAILING);
        lbRectangle.setSize(110, 30);
        lbRectangle.setLocation(5, 180);
        lbRectangle.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 20));
        this.add(lbRectangle);

        txtW = new JTextField("Width (w)");
        txtW.setForeground(Color.GRAY);
        txtW.setSize(100, 30);
        txtW.setLocation(120, 180);
        txtW.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                if (txtW.getText().equals("Width (w)")) {
                    txtW.setText("");

                }
            }

            @Override
            public void focusLost(FocusEvent e) {
                if (txtW.getText().isEmpty()) {
                    txtW.setForeground(Color.GRAY);
                    txtW.setText("Width (w)");
                }
            }
        });
        this.add(txtW);

        txtL = new JTextField("Lenght (l)");
        txtL.setForeground(Color.GRAY);
        txtL.setSize(100, 30);
        txtL.setLocation(230, 180);
        txtL.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                if (txtL.getText().equals("Lenght (l)")) {
                    txtL.setText("");

                }
            }

            @Override
            public void focusLost(FocusEvent e) {
                if (txtL.getText().isEmpty()) {
                    txtL.setForeground(Color.GRAY);
                    txtL.setText("Lenght (l)");
                }
            }
        });
        this.add(txtL);

        btnCal = new JButton("Calculate");
        btnCal.setSize(100, 30);
        btnCal.setLocation(370, 180);
        btnCal.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strWidht = txtW.getText();
                    String strlenght = txtL.getText();
                    double w = Double.parseDouble(strWidht);
                    double l = Double.parseDouble(strlenght);
                    Rectangle rectangle = new Rectangle(w, l);
                    lbRectangleout.setText("Rectangle: Widht = " + String.format("%.2f", w)
                            + " lenght  = " + String.format("%.2f", l)
                            + " Area = " + String.format("%.2f", rectangle.calArea())
                            + " Perimeter = " + String.format("%.2f", rectangle.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(Shapecalculator.this, "Error: Please input number!",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    txtW.setText("");
                    txtL.setText("");
                    txtW.requestFocus();
                }
            }
        });

        this.add(btnCal);

        lbRectangleout = new JLabel("Rectangle : Lenght = ..........   Width = .......... Area = .......... Perimeter = ..........");
        lbRectangleout.setSize(700, 30);
        lbRectangleout.setLocation(30, 220);
        lbRectangleout.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 15));
        this.add(lbRectangleout);

        //Square
        lbSquare = new JLabel("Square :", JLabel.TRAILING);
        lbSquare.setSize(110, 30);
        lbSquare.setLocation(5, 260);
        lbSquare.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 20));
        lbSquare.setText("Side");
        this.add(lbSquare);

        txtS = new JTextField("Side (s)");
        txtS.setForeground(Color.GRAY);
        txtS.setSize(100, 30);
        txtS.setLocation(120, 260);
        txtS.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                if (txtS.getText().equals("Side (s)")) {
                    txtS.setText("");

                }
            }

            @Override
            public void focusLost(FocusEvent e) {
                if (txtS.getText().isEmpty()) {
                    txtS.setForeground(Color.GRAY);
                    txtS.setText("Side (s)");
                }
            }
        });
        this.add(txtS);

        btnCal = new JButton("Calculate");
        btnCal.setSize(100, 30);
        btnCal.setLocation(370, 260);
        btnCal.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strSide = txtS.getText();
                    double side = Double.parseDouble(strSide);
                    Square square = new Square(side);
                    lbSquareout.setText("Square: Side = " + String.format("%.2f", side)
                            + " Area = " + String.format("%.2f", square.calArea())
                            + " Perimeter = " + String.format("%.2f", square.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(Shapecalculator.this, "Error: Please input number!",
                             "Error", JOptionPane.ERROR_MESSAGE);
                    txtS.setText("");
                    txtS.requestFocus();
                }
            }
        });

        this.add(btnCal);

        lbSquareout = new JLabel("Square : Side = ..........  Area = .......... Perimeter = ..........");
        lbSquareout.setSize(700, 30);
        lbSquareout.setLocation(30, 300);
        lbSquareout.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 15));
        this.add(lbSquareout);

        /////ETC.
        btnClear = new JButton("Clear");
        btnClear.setSize(150, 60);
        btnClear.setLocation(15, 500);
        btnClear.setBackground(Color.gray);
        this.add(btnClear);
        btnClear.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                txtR.setText("Radius (r)");
                txtH.setText("Height (h)");
                txtB.setText("Base (b)");
                txtW.setText("Width (w)");
                txtL.setText("Lenght (l)");
                txtS.setText("Side (s)");

            }
        });

        btnCal_All = new JButton("Calculate All");
        btnCal_All.setSize(150, 60);
        btnCal_All.setLocation(185, 500);
        btnCal_All.setBackground(Color.green);
        btnCal_All.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strSide = txtS.getText();
                    double side = Double.parseDouble(strSide);
                    Square square = new Square(side);
                    lbSquareout.setText("Square: Side = " + String.format("%.2f", side)
                            + " Area = " + String.format("%.2f", square.calArea())
                            + " Perimeter = " + String.format("%.2f", square.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(Shapecalculator.this, "Error: Please input number!",
                             "Error", JOptionPane.ERROR_MESSAGE);
                    txtS.setText("");
                    txtS.requestFocus();
                }
            }
        });

        btnCal_All.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strWidht = txtW.getText();
                    String strlenght = txtL.getText();
                    double w = Double.parseDouble(strWidht);
                    double l = Double.parseDouble(strlenght);
                    Rectangle rectangle = new Rectangle(w, l);
                    lbRectangleout.setText("Rectangle: Widht = " + String.format("%.2f", w)
                            + " lenght  = " + String.format("%.2f", l)
                            + " Area = " + String.format("%.2f", rectangle.calArea())
                            + " Perimeter = " + String.format("%.2f", rectangle.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(Shapecalculator.this, "Error: Please input number!",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    txtW.setText("");
                    txtL.setText("");
                    txtW.requestFocus();
                }
            }
        });

        btnCal_All.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strBase = txtH.getText();
                    String strHeight = txtB.getText();
                    double h = Double.parseDouble(strBase);
                    double b = Double.parseDouble(strHeight);
                    Triangle triangle = new Triangle(h, b);
                    lbTriangleout.setText("Triangle: Base = " + String.format("%.2f", b)
                            + " Height = " + String.format("%.2f", h)
                            + " Area = " + String.format("%.2f", triangle.calArea())
                            + " Perimeter = " + String.format("%.2f", triangle.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(Shapecalculator.this, "Error: Please input number!",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    txtB.setText("");
                    txtH.setText("");
                    txtB.requestFocus();
                }
            }
        });

        btnCal_All.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strRadius = txtR.getText();
                    double r = Double.parseDouble(strRadius);
                    Circle circle = new Circle(r);
                    lbCircleout.setText("Circle: Radius = " + String.format("%.2f", r)
                            + " Area = " + String.format("%.2f", circle.calArea())
                            + " Perimeter = " + String.format("%.2f", circle.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(Shapecalculator.this, "Error: Please input number!",
                             "Error", JOptionPane.ERROR_MESSAGE);
                    txtR.setText("");
                    txtR.requestFocus();
                }
            }
        });
        this.add(btnCal_All);

        btnExit = new JButton("EXIT");
        btnExit.setSize(150, 60);
        btnExit.setLocation(350, 500);
        btnExit.setBackground(Color.red);
        btnExit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        this.add(btnExit);

    }

    public static void main(String[] args) {
        Shapecalculator frame = new Shapecalculator();
        frame.setVisible(true);

    }
}
