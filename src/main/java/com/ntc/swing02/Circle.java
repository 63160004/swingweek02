/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ntc.swing02;

/**
 *
 * @author L4ZY
 */
public class Circle extends Shape {

    private double r;
    public Circle(double r) {

        this.r = r;

    }

    public void setR(double r) {
        this.r = r;
    }

    public double getR() {
        return r;
    }

    @Override
    public double calArea() {
        return Math.PI * Math.pow(r, 2);

    }

    @Override
    public double calPerimeter() {
        return 0.5 * Math.PI * r;
    }

}
